Marionette = require('backbone.marionette')
AlbumCollection = require('../collections/album')
Album = Marionette.ItemView.extend(
  template: require('../templates/album.html')
  tagName: 'div')
AlbumList = Marionette.CollectionView.extend(
  childView: Album
  tagName: 'div'
  className: 'row'
  collection: AlbumCollection)
module.exports = AlbumList