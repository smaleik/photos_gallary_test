Marionette = require('backbone.marionette')
LayoutView = Marionette.LayoutView.extend(
  template: require('../templates/layout.html')
  regions:
    layout: '.app-hook')
module.exports = LayoutView