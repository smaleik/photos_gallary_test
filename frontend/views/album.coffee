Marionette = require('backbone.marionette')
PhotoList = require('../collections/photo')
PhotoView = Marionette.ItemView.extend(
  tagName: 'div'
  className: 'col-xs-6 col-md-3'
  template: require('../templates/photo.html'))
PhotoListView = Marionette.CompositeView.extend(
  template: require('../templates/photos.html')
  childViewContainer: '#photos'
  collection: PhotoList
  childView: PhotoView
  templateHelpers: ->
    {
      total_items: @collection.total_items
      current_page: @collection.current_page
      per_page: @collection.per_page
      album_id: @collection.album_id
    }
)
module.exports = PhotoListView