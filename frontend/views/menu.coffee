Marionette = require('backbone.marionette')
Menu = Marionette.ItemView.extend(
  template: require('../templates/menu.html')
  tagName: 'div')
module.exports = Menu