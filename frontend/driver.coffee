Marionette = require('backbone.marionette')
Router = require('./routers/album')
#require('./assets/css/bootstrap.css')

App = new (Marionette.Application)(onStart: (options) ->
    new Router(options)

    ###* Starts the URL handling framework ###

    Backbone.history.start()
    return
)
App.start()