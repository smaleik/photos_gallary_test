Album = require('../models/album');
Backbone = require('backbone');

module.exports = Backbone.Collection.extend({
    model: Album,
    url: '/albums'
});