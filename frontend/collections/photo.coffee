Photo = require('../models/photo');
Backbone = require('backbone');

module.exports = Backbone.Collection.extend({
    defaults: {
        current_page: 1,
        total_items: '',
        per_page: '',
        album_id: ''
    },
    model: Photo,
    url: '/album/photos',
    parse: (response) ->
        this.per_page = response.itemsPerPage;
        this.total_items = response.totalItems;
        this.current_page = response.currentPage;
        return response.data;
});