Marionette = require('backbone.marionette')
Backbone = require('backbone')
LayoutView = require('../views/layout')
AlbumList = require('../collections/album')
MenuView = require('../views/menu')
PhotoCollection = require('../collections/photo')
PhotoListView = require('../views/album')
AlbumListView = require('../views/albumList')
Promise = require('promise')
Spinner = require('../common/spinner')
Controller = Marionette.Object.extend(
  initialize: ->
    @options.regionManager = new (Marionette.RegionManager)(regions:
      menu: '#menu-hook'
      main: '#main-hook')
    layout = new LayoutView
    @getOption('regionManager').get('menu').show new MenuView
    @getOption('regionManager').get('main').show layout
    @options.layout = layout
    return
## Получаем альбомы
  albumList: ->
    layout = @getOption('layout')
    layout.showChildView 'layout', new Spinner
    albums = new AlbumList
    promise = @promise(albums, {})
    promise.then (albums) ->
      console.log albums
      layout.showChildView 'layout', new AlbumListView(collection: albums)
      Backbone.history.navigate ''
      return
    return
## Получаем фотографии альбома
  albumView: (album_id, page) ->
    layout = @getOption('layout')
    layout.showChildView 'layout', new Spinner
    if `page == 'undefined'` or `page == null`
      page = 1
    photos = new PhotoCollection
    photos.album_id = album_id
    promise = @promise(photos,
      album_id: album_id
      page: page)
    promise.then (photos) ->
      layout.showChildView 'layout', new PhotoListView(collection: photos)
      Backbone.history.navigate 'album/' + album_id + '/page/' + page
      return
    return
  promise: (collection, data) ->
    new Promise((resolve, reject) ->
      collection.fetch
        data: data
        success: (data) ->
          resolve data
          return
        error: ->
          resolve `undefined`
          return
      return
    )
)
module.exports = Controller