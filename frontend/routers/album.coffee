Marionette = require('backbone.marionette')
AlbumController = require('../controllers/Album')
Router = Marionette.AppRouter.extend(
    appRoutes:
        '': 'albumList'
        'album/:id': 'albumView'
        'album/:id/page/:page': 'albumView'
    initialize: ->
        @controller = new AlbumController
        return
)
module.exports = Router