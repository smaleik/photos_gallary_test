var webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'development';
module.exports = {
    entry: './driver',
    module: {
        loaders: [
            {
                test: /\.html$/,
                loader: 'underscore-template-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.coffee$/,
                loader: 'coffee-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(coffee\.md|litcoffee)$/,
                loader: "coffee-loader?literate"
            },
            {
                test: /\.css$/, loader: "style!css"
            }
        ]
    },
    output: {
        path: __dirname + './../backend/web/bundles/frontend',
        filename: 'bundle.js'
    },
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: NODE_ENV == 'development' ? "cheap-inline-module-source-map" : null,
    plugins: [
        new webpack.ProvidePlugin({
            _: 'underscore'
        }),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery'
        }),
        new webpack.EnvironmentPlugin
    ],
    resolve: {
        modulesDirectories: [__dirname + '/node_modules'],
        root: __dirname,
        extensions: [".web.coffee", ".web.js", ".coffee", "", ".webpack.js", ".web.js", ".js"]
    },
    resolveLoader: {
        root: __dirname + '/node_modules'
    }
};

if (NODE_ENV == 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    )
}