<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Photo;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AlbumsLoadData
 * @package AppBundle\DataFixtures\ORM
 */
class AlbumsLoadData implements FixtureInterface, ContainerAwareInterface
{
    private $container;


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $count = 0;
        $faker = Factory::create();
        for ($i = 1; $i <= 5; $i++) {
            $album = new Album();
            $album->setTitle($faker->sentence(5));
            $manager->persist($album);

            if ($i == 1) {
                $count = 5;
            } else {
                $count+= 20;
            }

            $this->loadPhotos($manager, $count, $album);
            $manager->flush();
        }
    }


    private function loadPhotos(ObjectManager $manager, $count = 0, Album $album)
    {
        $faker = Factory::create();
        for ($i = 1; $i <= $count; $i++) {
            $photo = new Photo();
            $photo->setTitle($faker->sentence(5));
            $photo->setSrc($faker->image($this->container->getParameter('web_dir') .'/images', '300', '300', 'cats', false));
            $photo->setAlbum($album);
            $manager->persist($photo);
            $manager->flush();
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}