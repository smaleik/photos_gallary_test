<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends FOSRestController
{
    /**
     * Отдает список альбомов а последними 10 фотографиями к каждому альбому
     * @return mixed
     */
    public function getAlbumsAction()
    {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('AppBundle:Album')->getAlbumsWithLastPhotos();
    }

    /**
     * Отдает фотографии в альбоме в виде json с пагинацией
     * @param Request $request
     * @return mixed
     */
    public function getAlbumPhotosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $photos_query = $em->getRepository('AppBundle:Photo')->getPhotosQuery($request->get('album_id'));
        return $this->get('knp_paginator')->paginate($photos_query, $request->get('page', 1), 10);
    }
}