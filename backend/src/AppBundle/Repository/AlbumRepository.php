<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AlbumRepository extends EntityRepository
{
    /**
     * Отдает список альбомов и последних фото к каждому
     * @param int $photos_limit
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAlbumsWithLastPhotos($photos_limit = 10)
    {
        $sql = 'SELECT
                  photo.id as photo_id,
                  photo.src as photo_src,
                  album.title as album_title,
                  album.id as album_id
                FROM (SELECT (@rn := if(@album_id = album_id, @rn + 1, if(@album_id := album_id, 1, 1))) AS rn,
                        photo.*
                      FROM photo
                        CROSS JOIN
                        (SELECT
                           @rn := 0,
                           @album_id := \'\') params
                     ) photo
                 LEFT JOIN album on photo.album_id = album.id
                WHERE rn <= ?
                ORDER BY album_id, photo_id DESC';

        $connection = $this->_em->getConnection();
        $statement = $connection->prepare($sql);
        $statement->bindValue(1, $photos_limit);
        $statement->execute();
        $results = $statement->fetchAll();

        $data = [];
        $album_id = 0;
        foreach ($results as $row) {
            if ($row['album_id'] !== $album_id) {
                $data[] = [
                    'id' => $row['album_id'],
                    'title' => $row['album_title'],
                    'photos' => $this->getPhotos($results, $row['album_id'])
                ];
                $album_id = $row['album_id'];
            }
        }

        return $data;
    }

    private function getPhotos($results, $album_id)
    {
        $photos = [];
        foreach ($results as $row) {
            if ($row['album_id'] == $album_id) {
                $photos[] = [
                    'id' => $row['photo_id'],
                    'src' => $row['photo_src']
                ];
            }
        }
        return $photos;
    }
}