<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class PhotoRepository extends EntityRepository
{
    public function getPhotosQuery($album_id)
    {
        return $this->_em->createQuery('SELECT p.id, p.src FROM AppBundle:Photo p WHERE p.album_id = :album_id')->setParameter('album_id', $album_id);
    }
}